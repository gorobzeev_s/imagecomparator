package com.test.service;

import org.springframework.web.multipart.MultipartFile;

public interface CompareService {

    String saveImage(MultipartFile multipartFile);

    byte[] compareTwoImages(String firstFileName, String secondFileName);
}
