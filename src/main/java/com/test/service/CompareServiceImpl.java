package com.test.service;

import com.test.exception.CommonProblemException;
import com.test.model.ChangeScope;
import com.test.model.Pixel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class CompareServiceImpl implements CompareService {

    private final static Logger logger = LoggerFactory.getLogger(CompareServiceImpl.class);

    private static final String BASE_TEMP_PATH = System.getProperty("java.io.tmpdir");

    private static final double DEFAULT_DISTANCE_NEIGHBOR = 10.0;

    @Override
    public String saveImage(MultipartFile multipartFile) {
        logger.info("Folder for save - {}", BASE_TEMP_PATH + "/" + multipartFile.getOriginalFilename());
        savePhotoToServer(BASE_TEMP_PATH + "/" + multipartFile.getOriginalFilename(), multipartFile);
        return multipartFile.getOriginalFilename();
    }

    @Override
    public byte[] compareTwoImages(String firstFileName, String secondFileName) {
        byte[] result = null;
        try {
            File file1 = new File(BASE_TEMP_PATH + "/" + firstFileName);
            File file2 = new File(BASE_TEMP_PATH + "/" + secondFileName);

            BufferedImage firstBufferedImage = ImageIO.read(file1);
            BufferedImage secondBufferedImage = ImageIO.read(file2);

            if (!compareByHeightWidth(firstBufferedImage, secondBufferedImage)) {
                throw new CommonProblemException("Images has different size");
            }

            if (isImageEquals(file1, file2)) {
                throw new CommonProblemException("Images are full equal");
            }

            List<Pixel> pixelList = getCountDifferentPixels(firstBufferedImage, secondBufferedImage);
            List<List<Pixel>> pixelsRegions = groupListPixels(pixelList);
            List<ChangeScope> scopes = new ArrayList<>();

            for (List<Pixel> pixelsRegion : pixelsRegions) {
                scopes.add(makeScope(pixelsRegion));
            }

            Graphics2D g2d = secondBufferedImage.createGraphics();
            for (ChangeScope scope : scopes) {
                int minX = scope.getMinX();
                int maxX = scope.getMaxX();
                int minY = scope.getMinY();
                int maxY = scope.getMaxY();
                g2d.setColor(Color.RED);
                g2d.drawLine(minX, minY, maxX, minY);
                g2d.drawLine(maxX, minY, maxX, maxY);
                g2d.drawLine(minX, maxY, maxX, maxY);
                g2d.drawLine(minX, minY, minX, maxY);
            }
            g2d.dispose();

            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            ImageIO.write(secondBufferedImage, "jpg", bao);
            result = bao.toByteArray();

        } catch (IOException e) {
            throw new CommonProblemException("Failed to compare image files ...");
        }
        return result;
    }

    private boolean compareByHeightWidth(BufferedImage first, BufferedImage second) {
        return first.getHeight() == second.getHeight() && first.getWidth() == second.getWidth();
    }

    private List<Pixel> getCountDifferentPixels(BufferedImage firstImage, BufferedImage secondImage) {
        List<Pixel> result = new ArrayList<>();
        for (int y = 0; y < firstImage.getHeight(); y++) {
            for (int x = 0; x < firstImage.getWidth(); x++) {
                if (firstImage.getRGB(x, y) != secondImage.getRGB(x, y)) {
                    result.add(new Pixel(x, y));
                }
            }
        }
        return result;
    }


    private boolean isImageEquals(File fileA, File fileB) {

        float percentage = 0;
        try {
            BufferedImage bufferedImageFirst = ImageIO.read(fileA);
            DataBuffer dataBufferImageFirst = bufferedImageFirst.getData().getDataBuffer();
            int sizeA = dataBufferImageFirst.getSize();
            BufferedImage bufferedImageSecond = ImageIO.read(fileB);
            DataBuffer dataBufferImageSecond = bufferedImageSecond.getData().getDataBuffer();
            int sizeB = dataBufferImageSecond.getSize();
            int count = 0;
            if (sizeA == sizeB) {
                for (int i = 0; i < sizeA; i++) {
                    if (dataBufferImageFirst.getElem(i) == dataBufferImageSecond.getElem(i)) {
                        count = count + 1;
                    }
                }
                percentage = (count * 100) / sizeA;
            }
        } catch (IOException e) {
            throw new CommonProblemException("Failed to compare image files ...");
        }
        return percentage == 100.0;
    }


    private List<List<Pixel>> groupListPixels(List<Pixel> pixels) {
        List<List<Pixel>> result = new ArrayList<>();

        for (Pixel pixel : pixels) {
            if (result.isEmpty()) {
                List<Pixel> newColumn = new ArrayList<>();
                newColumn.add(pixel);
                result.add(newColumn);
            } else {
                determinePixel(pixel, result);
            }
        }
        return result;
    }

    private void determinePixel(Pixel currentPixel, List<List<Pixel>> result) {
        boolean isNeighborWasFound = false;
        for (List<Pixel> pixels : result) {
            for (Pixel pixel : pixels) {
                if (arePixelsNeighbors(pixel, currentPixel)) {
                    pixels.add(currentPixel);
                    isNeighborWasFound = true;
                    break;
                }
            }
        }

        if (!isNeighborWasFound) {
            List<Pixel> newCol = new ArrayList<>();
            newCol.add(currentPixel);
            result.add(newCol);
        }
    }

    private boolean arePixelsNeighbors(Pixel p1, Pixel p2) {
        int x1 = p1.getX();
        int y1 = p1.getY();

        int x2 = p2.getX();
        int y2 = p2.getY();

        double distance = Math.sqrt((x2 * x2 - 2 * x1 * x2 + x1 * x1) + (y2 * y2 - 2 * y1 * y2 + y1 * y1));
        return distance < DEFAULT_DISTANCE_NEIGHBOR;
    }

    private ChangeScope makeScope(List<Pixel> pixels) {
        int minX = pixels.get(0).getX();
        int maxX = pixels.get(0).getX();
        int minY = pixels.get(0).getY();
        int maxY = pixels.get(0).getY();

        for (Pixel pixel : pixels) {

            if (pixel.getX() < minX) {
                minX = pixel.getX();
            }
            if (pixel.getX() > maxX) {
                maxX = pixel.getX();
            }

            if (pixel.getY() < minY) {
                minY = pixel.getY();
            }
            if (pixel.getY() > maxY) {
                maxY = pixel.getY();
            }
        }
        return new ChangeScope(maxX, minX, maxY, minY);
    }

    private void savePhotoToServer(String path, MultipartFile uploadedFileRef) {
        byte[] buffer = new byte[1000];
        File outputFile = new File(path);
        FileInputStream reader = null;
        FileOutputStream writer = null;
        int totalBytes = 0;
        try {
            outputFile.createNewFile();
            reader = (FileInputStream) uploadedFileRef.getInputStream();
            writer = new FileOutputStream(outputFile);
            int bytesRead = 0;
            while ((bytesRead = reader.read(buffer)) != -1) {
                writer.write(buffer);
                totalBytes += bytesRead;
            }
        } catch (IOException e) {
            logger.warn("Error saving photo");
            throw new CommonProblemException("Error saving photo");
        } finally {
            try {
                reader.close();
                writer.close();
            } catch (IOException e) {
                logger.warn("Error saving photo");
                throw new CommonProblemException("Error saving photo");
            }
        }
    }

}
