package com.test.controller;

import com.test.exception.CommonProblemException;
import com.test.model.ResponseCustomError;
import com.test.model.ResponseModel;
import com.test.service.CompareService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1", method = RequestMethod.GET)
public class ApiController {

    private final static Logger logger = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    private CompareService compareService;

    @RequestMapping(value = "/save", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map saveFirstPhoto(@RequestParam MultipartFile photo) {
        logger.info("Entering savePhoto()");
        if (photo == null || photo.isEmpty()) {
            logger.warn("File is empty error");
            throw new ConstraintViolationException("file is empty", null);
        }
        Map<String, String> result = new HashMap<>();
        String fileName = compareService.saveImage(photo);
        result.put("data", fileName);
        return result;
    }

    @RequestMapping(value = "/get_image", method = RequestMethod.GET, produces = "image/jpg")
    public byte[] getImage(@RequestParam String first, @RequestParam String second) {
        logger.info("Entering getImage() first {}, second {}", first, second);
        byte[] response = compareService.compareTwoImages(first, second);
        logger.info("Leaving getUserImage()");
        return response;
    }

    @ExceptionHandler(value = {CommonProblemException.class})
    public ResponseModel<?> handle(CommonProblemException e) {
        ResponseModel<?> errorData = new ResponseModel<>();
        ResponseCustomError error = new ResponseCustomError();
        error.setMessage(e.getMessage());
        errorData.setError(error);
        return errorData;
    }
}
