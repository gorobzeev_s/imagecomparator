App.service('service', function (config, $http, ngNotify) {

    console.log('core service initialized');

    this.uploadPhoto = function (targetUrl, body, resp) {
        $http({
            url: targetUrl,
            method: 'POST',
            headers: {
                'Content-Type': undefined
            },
            data: body
        })
            .then(function successCallback(response) {
                resp(response.data);
            }, function errorCallback(response) {
                alert('Something went wrong');
            });
    };

    this.getImage = function (targetUrl, resp) {
        $http({
            method: 'GET',
            url: targetUrl,
            responseType: 'arraybuffer'
        }).then(function (response) {
            console.log(response);
            var str = _arrayBufferToBase64(response.data);
            console.log(str);
            // str is base64 encoded.
            resp(str);
        }, function (response) {
            console.error('error in getting static img.');
            var error = 'some problem';
            resp(error);
        });
    };

    function _arrayBufferToBase64(buffer) {
        var binary = '';
        var bytes = new Uint8Array(buffer);
        var len = bytes.byteLength;
        for (var i = 0; i < len; i++) {
            binary += String.fromCharCode(bytes[i]);
        }
        return window.btoa(binary);
    }

});
