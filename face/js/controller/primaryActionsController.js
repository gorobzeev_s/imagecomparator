App.controller('primaryActionsController', function ($http, config, $scope, $window, service, ngNotify, ngProgressFactory) {
    console.log('core primaryActionsController initialized');

    $scope.progressbar = ngProgressFactory.createInstance();

    var saveFileUrl = config.url_basic + config.save_image;
    var loadImage = config.url_basic + config.load_image;

    $scope.first_name_file = undefined;
    $scope.second_name_file = undefined;
    $scope.image_load = undefined;
    $scope.image_identify = undefined;

    var formdata = new FormData();
    $scope.getTheFilesForFirstFile = function ($files) {
        angular.forEach($files, function (value, key) {
            formdata.append('photo', value);
        });
    };

    $scope.uploadFiles = function () {
        $scope.progressbar.start();
        service.uploadPhoto(saveFileUrl, formdata, function (data) {
            $scope.first_name_file = data.data;
            console.log('Set first file name key - ' + $scope.first_name_file);
            var entries = formdata.entries();
            for (var pair of entries) {
                formdata.delete(pair[0]);
            }
            $scope.progressbar.complete();
        });
    };

    var formdatasecond = new FormData();
    $scope.getTheFilesForSecondFile = function ($files) {
        angular.forEach($files, function (value, key) {
            formdatasecond.append('photo', value);
        });
    };

    $scope.uploadSecondFiles = function () {
        $scope.progressbar.start();
        service.uploadPhoto(saveFileUrl, formdatasecond, function (data) {
            $scope.second_name_file = data.data;
            console.log('Set second file name key - ' + $scope.second_name_file);
            var entries = formdatasecond.entries();
            for (var pair of entries) {
                formdatasecond.delete(pair[0]);
            }
            $scope.progressbar.complete();
        });
    };

    $scope.getResultImage = function (first, second) {
        var url = loadImage + '?first=' + first + '&second=' + second;
        console.log('Url - ' + url);
        service.getImage(url, function (data) {
            if (data == 'some problem') {
                $scope.image_identify = true;
            } else {
                $scope.image_load = data;
            }
        });
    };
});