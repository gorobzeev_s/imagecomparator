App.module("services.interceptor", arguments).config(function($httpProvider) {
    $httpProvider.interceptors.push(function($q) {
        return {
            responseError: function(rejection) {
                if(rejection.status <= 0) {
                    window.location = "noresponse.html";
                    return;
                }
                return $q.reject(rejection);
            }
        };
    });
});
