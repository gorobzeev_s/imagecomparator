App.config(function ($locationProvider, $routeProvider) {

    $routeProvider
        .when('/info', {
            templateUrl: 'pages/info.html',
            controller: 'primaryActionsController'
        })
        .when('/comparator', {
            templateUrl: 'pages/comparator.html',
            controller: 'primaryActionsController'
        })
        .otherwise({
            redirectTo: '/info'
        });
});